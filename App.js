import React, { Component } from 'react';
import {
	AppRegistry,
	NativeEventEmitter,
	NativeModules,
	AppState,
	Button, Text,
	View
} from 'react-native';
import Geofence from "react-native-geofence";
import MapView from 'react-native-maps';

const GeofenceEmitter = new NativeEventEmitter(NativeModules.Geofence);

const interval = null;

export default class hisd3notifier extends Component {
	constructor(props) {
		super(props);

		// this.state = {
		//   region: {
		//     latitude: 9.627370,
		//     longitude: 123.880610,
		//     latitudeDelta: 0.0005,
		//     longitudeDelta: 0.0005
		//   },
		//   appState: AppState.currentState
		// }

		this.state = {
			region: {
				latitude: 37.33052610000001,
				longitude: -122.029450599999999,
				latitudeDelta: 0.02,
				longitudeDelta: 0.02
			},
			appState: AppState.currentState
		}
	}

	componentDidMount(){
		GeofenceEmitter.addListener('didEnterRegion', (reminder) => this.didEnterRegion());
		AppState.addEventListener('change', this._handleAppStateChange);
	}

	componentWillUnmount() {
		AppState.removeEventListener('change', this._handleAppStateChange);
	}

	_handleAppStateChange = (nextAppState) => {
		if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
			// clearInterval(interval);
		}
		this.setState({appState: nextAppState});
	}

	didEnterRegion=()=>{
		console.log('Succesful enter region')
		// interval = setInterval(function () {
		//   SpeechNotification.notify({
		//     title: 'Title',
		//     icon: 'icon', // {icon}.png/.jpg must be present in each corresponding android/app/src/main/res/drawable-*dpi/ folders
		//     message: "Master, Please time in NOW!",
		//     language: "en-US"
		//   });
		// }, 5000);
	}

	onRegionChange=()=>{
		return(region)=>{
			this.setState({region})
		}
	}

	startMonitoring=()=>{
		Geofence.startMonitoring({
			identifier: 'identifier',
			latitude: this.state.region.latitude,
			longitude: this.state.region.longitude,
			radius: 50
		});
	}

	stopMonitoring=()=>{
		Geofence.stopMonitoring({
			identifier: 'identifier',
			latitude: this.state.region.latitude,
			longitude: this.state.region.longitude,
			radius: 50
		});
	}

	render() {
		return (
            <View style={{flex: 1}}>
              <MapView
                  style={{flex: 1}}
                  region={this.state.region}
                  onRegionChange={this.onRegionChange}
                  mapType={'hybrid'}
                  showsUserLocation={true}
              >
                <MapView.Circle draggable
                                center={this.state.region}
                                radius={50}
                />
                <Button primary full onPress={this.startMonitoring} title={'Start"'} />
                <Button danger full onPress={this.stopMonitoring} title={'Stop'} />
              </MapView>
            </View>
		);
	}
}
